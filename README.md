# Column mapper PHP 7

It provides extensive mapping of the data structure to another another data structure by defined rules and functional changes.

  - PHP 7.0 and higher

# Used
### Add column
- addColumn('newColumnName', '@originColumnName');
- addColumn('newColumnName', 'constant');
### Add column function
- addColumnFunction('newColumnName', [$this, 'nameMethod']);
- addColumnFunction('newColumnName', [$this, 'nameMethod'], ["paramName" => "paramValue", ...]);
### Add column rename function 
- addColumnRenameFunction('newColumnName', [$this, 'nameMethod']);
- addColumnRenameFunction('newColumnName', [$this, 'nameMethod'],  ["paramName" => "paramValue", ...]);
### Remove column
- removeColumn('newColumnName');
- removeColumn('@originColumnName');

# License

MIT,
**Free Software, Hell Yeah!**