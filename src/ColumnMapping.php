<?php

namespace grafikstudiom\columnMapping;

use grafikstudiom\columnMapping\Mapping\Utils;
use grafikstudiom\columnMapping\Exception\MappingException;
use grafikstudiom\columnMapping\Exception\SkipException;

/**
 * Class ColumnMapping
 * @package MartinFiala\ColumnMapping
 */
class ColumnMapping
{

    /**
     * @var array
     */
    protected $column = [];

    /**
     * Sets of column for remove after mapping
     * @var array
     */
    protected $remove = [];

    /**
     * @var array
     */
    protected $function = [];

    /**
     * @var array
     */
    protected $renameFunction = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Mapped data
     * @var array
     */
    protected $dataMapped = [];

    /**
     * ColumnMapping constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->setData($data);
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }


    /**
     * @param string $mapped
     * @param string $origin
     */
    public function addColumn(string $mapped, string $origin)
    {
        $this->column[$mapped] = $origin;
    }

    /**
     * @param string $name
     */
    public function removeColumn(string $name)
    {
        if(Utils::isOriginColumn($name)){
            foreach($this->column as $mappedName => $column){
                if($column === $name){
                    $this->remove[] = $mappedName;
                }
            }
        }
        $this->remove[] = $name;
    }

    /**
     * Return Columns
     * @return array
     */
    public function getColumn(): array
    {
        return $this->column;
    }


    /**
     * @param string $mapped
     * @param array $function
     * @param array $param
     */
    public function addColumnFunction(string $mapped, array $function, array $param = [])
    {
        $this->function[$mapped] = ['function' => $function, 'param' => $param];
    }

    /**
     * Return column functions
     * @return array
     */
    public function getColumnFunction(): array
    {
        return $this->function;
    }


    /**
     * @param string $mapped
     * @param array $function
     * @param array $param
     */
    public function addColumnRenameFunction(string $mapped, array $function, array $param = [])
    {
        $this->renameFunction[$mapped] = ['function' => $function, 'param' => $param];
    }

    /**
     * Return column rename functions
     * @return array
     */
    public function getColumnRenameFunction(): array
    {
        return $this->renameFunction;
    }


    /**
     * @param array $origin
     * @param array $mapped
     * @return array
     */
    protected function processFunctionColumn(array $origin, array $mapped) : array
    {
        $return = [];
        foreach ($this->function as $columnName => $function){
            if(method_exists($function['function'][0], $function['function'][1])) {
                $return[$columnName]['value'] = call_user_func_array($function['function'], [$columnName, (string)@$mapped[$columnName], $origin, $function['param']]);
            }else{
                throw new MappingException(MappingException::ERROR_DEFINED_MAPPING_FUNCTION_IS_NOT_EXIST . $columnName . ' = ' . get_class($function['function'][0]) . '::' . $function['function'][1]);
            }
        }
        return $return;
    }

    /**
     * @param array $origin
     * @param array $mapped
     * @return array
     */
    protected function processRenameFunctionColumn(array $origin, array $mapped) : array
    {
        foreach ($this->renameFunction as $columnName => $function){
            if(method_exists($function['function'][0], $function['function'][1])) {
                $newColumnName = call_user_func_array($function['function'], [$columnName, @$mapped[$columnName], $origin, $function['param']]);
                $mapped[$newColumnName] = $mapped[$columnName];
                unset($mapped[$columnName]);
            }else{
                throw new MappingException(MappingException::ERROR_DEFINED_MAPPING_FUNCTION_IS_NOT_EXIST . $columnName . ' = ' . get_class($function['function'][0]) . '::' . $function['function'][1]);
            }
        }
        return $mapped;
    }


    /**
     * @return array
     */
    public function processMapping() : array
    {
        foreach($this->data as $key => $row){
            $this->dataMapped[$key]['origin'] = $row;

            foreach ($this->column as $mappedColumn => $originColumn) {

                if (Utils::isOriginColumn($originColumn)) {
                    /** This part is column mapper origin to mapped */
                    if(!isset($row[Utils::prepareOriginColumnName($originColumn)])){
                        throw new MappingException(MappingException::COLUMN_DOESNT_EXIST.": ". $originColumn);
                    }
                    
                    if(is_string($row[Utils::prepareOriginColumnName($originColumn)])) {
                        if($mappedColumn !== '') {
                            $this->dataMapped[$key]['mapped'][$mappedColumn] = Utils::escapeSpecificChar($row[Utils::prepareOriginColumnName($originColumn)]);
                        }
                    }else{
                        if($mappedColumn !== '') {
                            $this->dataMapped[$key]['mapped'][$mappedColumn] = $row[Utils::prepareOriginColumnName($originColumn)];
                        }
                    }
                } else {
                    /** This part is column constant */
                    $this->dataMapped[$key]['mapped'][$mappedColumn] = $originColumn;
                }
            }

            /** Add column function with data */
            foreach ($this->processFunctionColumn($row, $this->dataMapped[$key]['mapped']) as $mappedColumn => $data) {
                try {
                    if($mappedColumn !== '') {
                        $this->dataMapped[$key]['mapped'][$mappedColumn] = $data['value'];
                    }
                } catch (SkipException $e) {
                    unset($this->dataMapped[$key]);
                    continue 1;
                }
            }

            /** Rename column function with data */
            $this->dataMapped[$key]['mapped'] = $this->processRenameFunctionColumn($row, $this->dataMapped[$key]['mapped']);

            /** Remove column */
            foreach($this->remove as $removeColumn){
                unset($this->dataMapped[$key]['mapped'][$removeColumn]);
            }
        }

        return $this->dataMapped;
    }

    /**
     * Return mapped data
     * @return array
     */
    public function getMappedData() : array
    {
        $mapped = [];
        foreach($this->dataMapped as $key => $row){
            $mapped[$key] = $row['mapped'];
        }

        return $mapped;
    }

    /**
     * Return mapped data with origin data
     * @return array
     */
    public function getFullMappedData() : array
    {
        return $this->dataMapped;
    }
}