<?php

namespace grafikstudiom\columnMapping\Exception;

/**
 * Class SkipException
 * @package MartinFiala\ColumnMapping\Exception
 */
class SkipException extends \Exception
{

}