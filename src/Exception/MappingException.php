<?php

namespace grafikstudiom\columnMapping\Exception;

/**
 * Class MappingException
 * @package MartinFiala\ColumnMapping\Exception
 */
class MappingException extends \Exception
{
    const ERROR_DEFINED_MAPPING_FUNCTION_IS_NOT_EXIST = "Error defined mapping function is not exist: ";
    const COLUMN_DOESNT_EXIST = "Column doesn't exist";

}