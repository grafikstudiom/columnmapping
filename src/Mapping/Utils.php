<?php

namespace grafikstudiom\columnMapping\Mapping;

/**
 * Class Utils
 * @package MartinFiala\ColumnMapping\Mapping
 */
class Utils
{
    /**
     * Check has origin column mapping name char '@'
     * @param string $name
     * @return bool
     */
    static function isOriginColumn(string $name) : bool
    {
        return $name[0] === '@' ? true : false;
    }


    /**
     * Unset first char from origin column name '@'
     * @param $name
     * @return string
     */
    static function prepareOriginColumnName(string $name) : string
    {
        return substr($name, 1);
    }

    /**
     * Escape special char ["] and [']
     * @param string $text
     * @return string
     */
    static function escapeSpecificChar(string $text) : string
    {
        $find    = ['"', '\''];
        $replace = ['´', '´'];
        return str_replace($find, $replace, $text);
    }
}