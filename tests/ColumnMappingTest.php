<?php
namespace grafikstudiom\columnMappingTests;

use PHPUnit\Framework\TestCase;
use grafikstudiom\columnMapping\ColumnMapping;
use grafikstudiom\columnMapping\Exception\MappingException;


class ColumnMappingTest extends TestCase
{

    private $testColumn = [
        "AnotherColumn1" => "@Column1",
        "AnotherColumn2" => "@Column2",
        "AnotherColumn3" => "@Column3"
    ];

    private $testData = [
        [
            "Column1" => "Data1",
            "Column2" => "Data2",
            "Column3" => "Data3",
        ],
        [
            "Column1" => "Data4",
            "Column2" => "Data5",
            "Column3" => "Data6",
        ],
        [
            "Column1" => "Data7",
            "Column2" => "Data8",
            "Column3" => "Data9",
        ]
    ];

    private $finalData = [
        [
            "AnotherColumn1" => "Data1",
            "AnotherColumn2" => "Data2",
            "AnotherColumn3" => "Data3",
        ],
        [
            "AnotherColumn1" => "Data4",
            "AnotherColumn2" => "Data5",
            "AnotherColumn3" => "Data6",
        ],
        [
            "AnotherColumn1" => "Data7",
            "AnotherColumn2" => "Data8",
            "AnotherColumn3" => "Data9",
        ]
    ];

    private $cm;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->cm = new ColumnMapping();
    }

    public function testSetData()
    {
        $this->cm->setData($this->testData);
        $this->assertEquals($this->cm->getData(), $this->testData);
    }

    public function testSetMappedColumn()
    {
        foreach($this->testColumn as $originName => $mappedName){
            $this->cm->addColumn($originName, $mappedName);
        }

        $this->assertEquals($this->cm->getColumn(), $this->testColumn);
    }

    public function testSetMappedFunction()
    {
        $this->cm->addColumnFunction('Column1', array($this, 'userMappedFunction'), array('testParam' => 'valParam'));
        $this->assertEquals(count($this->cm->getColumnFunction()), 1);
    }

    public function testSetRenameFunction()
    {
        $this->cm->addColumnRenameFunction('Column2', array($this, 'userMappedFunction'), array('testParam' => 'valParam'));
        $this->assertEquals(count($this->cm->getColumnRenameFunction()), 1);
    }

    public function testMappedProcess()
    {
            $this->cm->setData($this->testData);
            foreach($this->testColumn as $originName => $mappedName){
                $this->cm->addColumn($originName, $mappedName);
            }
            $this->cm->processMapping();
            $this->assertTrue(true, "Test OK");
    }

    public function testNullMappedFullData()
    {
        $this->cm->setData($this->testData);
        foreach($this->testColumn as $originName => $mappedName){
            $this->cm->addColumn($originName, $mappedName);
        }
        $this->cm->processMapping();
        $this->assertNotEmpty($this->cm->getFullMappedData());
    }

    public function testNullMappedData()
    {
        $this->cm->setData($this->testData);
        foreach($this->testColumn as $originName => $mappedName){
            $this->cm->addColumn($originName, $mappedName);
        }
        $this->cm->processMapping();
        $this->assertNotEmpty($this->cm->getMappedData());
    }

    public function testMappedData()
    {
        $this->cm->setData($this->testData);
        foreach($this->testColumn as $originName => $mappedName){
            $this->cm->addColumn($originName, $mappedName);
        }
        $this->cm->processMapping();
        $this->assertEquals($this->cm->getMappedData(), $this->finalData);
    }

    /**
     * @param string $key
     * @param string $value
     * @param array $origin
     * @param array $param
     * @return string
     */
    public function userMappedFunction(string $key, string $value, array $origin, array $param) : string
    {
        return "returnDataFromMappedFunction";
    }
}
