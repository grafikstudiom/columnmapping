<?php

use PHPUnit\Framework\TestCase;
use grafikstudiom\columnMapping\Mapping\Utils;

class UtilsTest extends TestCase
{
    public function testIsOriginColumn()
    {
        $this->assertTrue(Utils::isOriginColumn("@ColumnName"));
    }

    public function testIsNotOriginColumn()
    {
        $this->assertFalse(Utils::isOriginColumn("ColumnName"));
    }

    public function testPrepareName()
    {
        $this->assertEquals(Utils::prepareOriginColumnName("@Column"), "Column");
    }

    public function testEscape()
    {
        $this->assertEquals(Utils::escapeSpecificChar("W\"W'"), "W´W´");
    }
}
